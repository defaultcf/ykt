require "config"
require "typetalk"

class Sender
  def initialize
    @configpath = "#{ENV['HOME']}/.config/ykt.yml"
    unless File.exists? @configpath
      raise "No setting file. Please read USAGE section in READE.md"
    end
    Config.load_and_set_settings(@configpath)

    Typetalk.configure do |config|
      config.client_id = Settings[:typetalk_client_id]
      config.client_secret = Settings[:typetalk_client_secret]
    end

    @api = Typetalk::Api.new
  end

  def send(filepath)
    message = File.open(filepath, "r").read
    @api.post_message(Settings[:typetalk_topic_id], message)
  end
end
