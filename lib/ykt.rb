require "ykt/version"
require "thor"
require "erb"
require "sender"

module Ykt
  class Main < Thor
    include Thor::Actions

    def initialize(*args)
      super
      @filepath = "/tmp/ykt-#{Time.now.strftime('%Y%m%d')}"
      @template = ERB.new(File.open("#{__dir__}/template.erb").read).result
    end

    attr_reader :filepath
    default_task :main

    desc "main", "Do main process"
    def main
      write
      if yes?("Do you wanna send? (y/n): ", :bold)
        send
      end
    end

    desc "write", "Write ykt!"
    def write
      begin
        unless File.exists?(@filepath)
          file = File.open(@filepath, "a")
          file.puts @template
          file.close
        end
        system("nvim #{@filepath}")
      rescue => error
        p error
      end
    end

    desc "send", "Send to Typetalk!"
    def send
      sender = Sender.new
      sender.send @filepath
    end
  end
end
