# YKT
[![pipeline status](https://gitlab.com/i544c/ykt/badges/master/pipeline.svg)](https://gitlab.com/i544c/ykt/commits/master)
[![coverage report](https://gitlab.com/i544c/ykt/badges/master/coverage.svg)](https://i544c.gitlab.io/ykt/coverage/)

You can write YKT(やったこと，感想，次やること) easy using this command.

## Installation
Use [specific_install](https://github.com/rdp/specific_install).
```
gem specific_install -l git@gitlab.com:i544c/ykt.git
```

## Usage
You have to create `~/.config/ykt.yml` before use.
```yaml
typetalk_client_id: xxxxx
typetalk_client_secret: xxxxx
typetalk_topic_id: xxxxx
```

Run `ykt` on your shell.
