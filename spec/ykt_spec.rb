RSpec.describe Ykt do
  before do
    @ykt = Ykt::Main.new
  end

  it "has a version number" do
    expect(Ykt::VERSION).not_to be nil
  end

  it "has a valid path" do
    expect(@ykt.filepath).to match(/\/tmp\/ykt-\d{8}/)
  end
end
